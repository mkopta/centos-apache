#!/bin/sh

yum install -y httpd
systemctl enable httpd
systemctl start httpd
sed -i 's/Listen 80/Listen 8080/g' /etc/httpd/conf/httpd.conf

systemctl stop firewalld
systemctl disable firewalld
